from django.urls import path

from pets import views
from . import views



urlpatterns = [
    path('signupdono/', views.signupdono, name='signupdono'),
    path('signupvet/', views.signupvet, name='signupvet'),
    path('signupclinica/', views.signupclinica, name='signupclinica'),
    path('choice/', views.ChoiceView.as_view(), name='choice'),

]