from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.urls import reverse
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import Group
from django.contrib.auth import authenticate, login
from pets.models import Pet
from django.views import generic
from django.contrib.auth.mixins import LoginRequiredMixin, PermissionRequiredMixin
from django.contrib.auth.decorators import login_required, permission_required, user_passes_test

@user_passes_test(lambda u: u.is_anonymous)
def signupdono(request):
    if request.method == 'POST':
        form = UserCreationForm(request.POST)
        if form.is_valid():
            user = form.save()
            user_group = Group.objects.get(name='Dono de Pet Grupo')
            user.groups.add(user_group)
            
            login(request, user)

            return HttpResponseRedirect(reverse('pets:createdono'))
    else:
        form = UserCreationForm()

    context = {'form': form}
    return render(request, 'accounts/signupdono.html', context)

@user_passes_test(lambda u: u.is_anonymous)
def signupvet(request):
    if request.method == 'POST':
        form = UserCreationForm(request.POST)
        if form.is_valid():
            user = form.save()
            user_group = Group.objects.get(name='Veterinario Grupo')
            user.groups.add(user_group)

            login(request, user)

            return HttpResponseRedirect(reverse('pets:createvet'))
    else:
        form = UserCreationForm()

    context = {'form': form}
    return render(request, 'accounts/signupvet.html', context)

@user_passes_test(lambda u: u.is_anonymous)
def signupclinica(request):
    if request.method == 'POST':
        form = UserCreationForm(request.POST)
        if form.is_valid():
            user = form.save()
            user_group = Group.objects.get(name='Clinica Grupo')
            user.groups.add(user_group)

            login(request, user)

            return HttpResponseRedirect(reverse('pets:createclinica'))
    else:
        form = UserCreationForm()

    context = {'form': form}
    return render(request, 'accounts/signupclinica.html', context)


class ChoiceView(generic.ListView):
    model = Pet
    template_name = 'accounts/choice.html'
    

