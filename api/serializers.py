from rest_framework import serializers

from pets.models import Enciclopedia, Categoria


class CategoriaSerializer(serializers.ModelSerializer):


    class Meta:
        model = Categoria
        fields = ['id', 'nome']


class EnciclopediaSerializer(serializers.ModelSerializer):
    

    class Meta:
        model = Enciclopedia
        fields = ['id', 'name', 'assunto', 'conteudo', 'tipocategoria']


