from django.urls import path

from .views import EnciclopediaList, ArtigoDetail, CategoriaDetail, CategoriaList


urlpatterns = [
    path('enciclopedia/<int:pk>/', ArtigoDetail.as_view()),
    path('enciclopedia/', EnciclopediaList.as_view()),
    path('categoria/<int:pk>/', CategoriaDetail.as_view()),
    path('categoria/', CategoriaList.as_view()),
]

