from django.shortcuts import render
from rest_framework import generics

from pets.models import Enciclopedia, Categoria
from .serializers import EnciclopediaSerializer, CategoriaSerializer


class EnciclopediaList(generics.ListCreateAPIView):
    queryset = Enciclopedia.objects.all()
    serializer_class = EnciclopediaSerializer


class ArtigoDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = Enciclopedia.objects.all()
    serializer_class = EnciclopediaSerializer


class CategoriaList(generics.ListCreateAPIView):
    queryset = Categoria.objects.all()
    serializer_class = CategoriaSerializer

class CategoriaDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = Categoria.objects.all()
    serializer_class = CategoriaSerializer



