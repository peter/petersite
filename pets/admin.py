from django.contrib import admin
from .models import Pet, Animal, Donopet, Veterinario, Clinica, Atividade, Historico, Enciclopedia, Categoria


admin.site.register(Pet)
admin.site.register(Animal)
admin.site.register(Donopet)
admin.site.register(Veterinario)
admin.site.register(Clinica)
admin.site.register(Atividade)
admin.site.register(Historico)
admin.site.register(Enciclopedia)
admin.site.register(Categoria)
