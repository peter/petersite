from django import forms
from django.forms import ModelForm
from .models import Pet , Animal, Donopet, Veterinario, Clinica, Atividade, Historico

class PetForm(ModelForm):
    class Meta:
        model = Pet
        fields = [
            'name',
            'tipo_sanguineo',
            'raca',
            'imagem',
            'data_de_nascimento',
            'tipo_animal',
        ]
        labels = {
            'name': 'Nome',
            'tipo_sanguineo': 'Tipo sanguíneo',
            'raca': 'Raça',
            'imagem': 'Foto do animal',
            'data_de_nascimento': 'Data de nascimento',
            'tipo_animal':'Espécie',
        }


class DonopetForm(ModelForm):
    class Meta:
        model = Donopet
        fields = [
            'nome',
            'endereco',
            'bairro',
            'municipio',
            'estado',
            'cpf',
            'celular',
            'genero',
            'data_de_nascimento',
            'imagem',
        ]
        labels = {
            'nome':'Nome',
            'endereco': 'Endereço',            
            'bairro': 'Bairro',
            'municipio': 'Município',
            'estado': 'Estado',
            'cpf': 'CPF',
            'celular':'Número de celular',
            'genero':'Gênero',
            'data_de_nascimento':'Data de nascimento',
            'imagem':'Link para sua foto de perfil',
        }

class VeterinarioForm(ModelForm):
    class Meta:
        model = Veterinario
        fields = [
            'nome',
            'endereco',
            'bairro',
            'municipio',
            'estado',
            'cpf',
            'celular',
            'genero',
            'data_de_nascimento',
            'imagem',
            'agenda',
        ]
        labels = {
            'nome':'Nome',
            'endereco': 'Endereço',
            'bairro': 'Bairro',
            'municipio': 'Município',
            'estado': 'Estado',
            'cpf': 'CPF',
            'celular':'Número de celular',
            'genero':'Gênero',
            'data_de_nascimento':'Data de nascimento',
            'imagem':'Link para sua foto de perfil',
            'agenda':'Agenda'
        }

class ClinicaForm(ModelForm):
    class Meta:
        model = Clinica
        fields = [
            'nome',
            'endereco',
            'bairro',
            'municipio',
            'estado',
            'cnpj',
            'telefone',
            'cnae',
            'razao_social',
            'imagem',
            'nome_fantasia',
        ]
        labels = {
            'nome':'Nome',
            'endereco': 'Endereço',
            'bairro': 'Bairro',
            'municipio': 'Município',
            'estado': 'Estado',
            'cnpj': 'CNPJ',
            'telefone':'Número de telefone',
            'cnae':'CNAE',
            'razao_social':'Razão Social',
            'imagem':'Link para sua foto de perfil',
            'nome_fantasia':'Nome Fantasia'
        }

class AtividadeForm(ModelForm):
    class Meta:
        model = Atividade
        fields = [
            'nome',
            'data',
            'mensagem',
            'vet',
            'clinica',

        ]
        labels = {
            'nome':'Nome da atividade',
            'data': 'Data da atividade',
            'mensagem': 'Mensagem',
            'vet':"Selecione o veterinário, se quiser",
            'clinica':'Selecione a clínica, se quiser',
        }
class HistoricoForm(ModelForm):
    class Meta:
        model = Historico
        fields = [
            'texto',
            'data'
        ]
        labels = {
            'texto' : 'Texto',
            'data':'Data do ocorrido'

        }


class VeterinarioparadonoForm(ModelForm):
    class Meta:
        model = Veterinario
        fields = [
           'dono'
        ]
        widgets = {
            'dono': forms.HiddenInput,
        }