# Generated by Django 4.0 on 2021-12-12 17:04

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('pets', '0009_atividade'),
    ]

    operations = [
        migrations.AlterField(
            model_name='atividade',
            name='clinica',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='pets.clinica'),
        ),
        migrations.AlterField(
            model_name='atividade',
            name='vet',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='pets.veterinario'),
        ),
    ]
