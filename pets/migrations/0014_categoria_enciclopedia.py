# Generated by Django 4.0 on 2021-12-13 14:17

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('pets', '0013_merge_20211212_1606'),
    ]

    operations = [
        migrations.CreateModel(
            name='Categoria',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nome', models.CharField(max_length=255)),
            ],
        ),
        migrations.CreateModel(
            name='Enciclopedia',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('titulo', models.CharField(blank=True, max_length=255)),
                ('assunto', models.CharField(blank=True, max_length=255)),
                ('conteudo', models.TextField(blank=True)),
                ('tipo_categoria', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='pets.categoria')),
            ],
        ),
    ]
