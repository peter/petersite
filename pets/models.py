from django.db import models
from django.conf import settings
from datetime import datetime
from django.urls import reverse

class Animal(models.Model):
    
    name = models.CharField(max_length=255)

    def __str__(self):
        return f'{self.name}'

class Donopet(models.Model):
    usuario = models.OneToOneField(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    nome = models.CharField(max_length=255, blank=True)
    endereco = models.CharField(max_length=255, blank=True)
    bairro = models.CharField(max_length=255, blank=True)
    municipio = models.CharField(max_length=255, blank=True)
    estado = models.CharField(max_length=255, blank=True)
    cpf = models.CharField(max_length=255, blank=True)
    celular = models.CharField(max_length=255, blank=True)
    genero = models.CharField(max_length=255, blank=True)
    data_de_nascimento = models.DateTimeField()
    imagem = models.CharField(max_length=255)

    class Meta:
        permissions = [
            ("criar_pet","Pode criar um pet"),
            ("ver_dono","Pode ver páginas exclusivas de um dono de pet"),
            ("dono_ver_pet", "Dono pode ver seus pets"),
            ("preencher_forms_completar_signup_dono", "Pode preencher o forms com as informações de um dono de pet"),
        ]

    def __str__(self):
        return f'{self.usuario}'


class Veterinario(models.Model):
    usuario = models.OneToOneField(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    nome = models.CharField(max_length=255, blank=True)
    endereco = models.CharField(max_length=255, blank=True)
    bairro = models.CharField(max_length=255, blank=True)
    municipio = models.CharField(max_length=255, blank=True)
    estado = models.CharField(max_length=255, blank=True)
    cpf = models.CharField(max_length=255, blank=True)
    celular = models.CharField(max_length=255, blank=True)
    genero = models.CharField(max_length=255, blank=True)
    data_de_nascimento = models.DateTimeField()
    imagem = models.CharField(max_length=255)
    agenda = models.CharField(max_length=255, blank=True)
    dono = models.ManyToManyField(Donopet)

    class Meta:
        permissions = [
            ("vet_ver_pet", "Vet pode ver seus pets"),
            ("ver_vet","Pode ver páginas exclusivas de um veterinario"),
            ("preencher_forms_completar_signup_vet", "Pode preencher o forms com as informações de um veterinario"),
        ]

    def __str__(self):
        return f'{self.usuario}'


class Clinica(models.Model):
    usuario = models.OneToOneField(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    nome = models.CharField(max_length=255, blank=True)
    endereco = models.CharField(max_length=255, blank=True)
    bairro = models.CharField(max_length=255, blank=True)
    municipio = models.CharField(max_length=255, blank=True)
    estado = models.CharField(max_length=255, blank=True)
    cnpj = models.CharField(max_length=255, blank=True)
    telefone = models.CharField(max_length=255, blank=True)
    cnae = models.CharField(max_length=255, blank=True)
    razao_social = models.CharField(max_length=255, blank=True)
    imagem = models.CharField(max_length=255)
    nome_fantasia = models.CharField(max_length=255, blank=True)
    vet = models.ManyToManyField(Veterinario)

    class Meta:
        permissions = [
            ("clinica_ver_pet", "Clinica pode ver seus pets"),
            ("ver_clinica","Pode ver páginas exclusivas de uma clinica"),
            ("preencher_forms_completar_signup_clinica", "Pode preencher o forms com as informações de uma clinica"),
        ]

    def __str__(self):
        return f'{self.usuario}'

class Pet(models.Model):
    dono = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    name = models.CharField(max_length=255)
    tipo_sanguineo = models.CharField(max_length=255, blank=True)
    raca = models.CharField(max_length=255, blank=True)
    imagem = models.URLField(max_length=255, null=True, blank=True)
    data_de_nascimento = models.DateTimeField()

    tipo_animal = models.ForeignKey(
        Animal,
        on_delete=models.CASCADE,
    )

    def __str__(self):
        return f'{self.name}'

class Historico(models.Model):
    editor = models.ForeignKey(settings.AUTH_USER_MODEL,
                               on_delete=models.CASCADE)
    texto = models.CharField(max_length=255)
    data = models.DateTimeField(default=datetime.now)
    pet = models.ForeignKey(Pet, on_delete=models.CASCADE)
    
    def __str__(self):
        return f'{self.data} - ({self.editor}) {self.texto}'


class Atividade(models.Model):
    usuario = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    vet = models.ForeignKey(Veterinario, on_delete=models.CASCADE, blank=True, null=True)
    clinica = models.ForeignKey(Clinica, on_delete=models.CASCADE, blank=True, null=True)

    nome = models.CharField(max_length=255, blank=True)
    data = models.DateTimeField()
    mensagem = models.CharField(max_length=255, blank=True)


    def __str__(self):
        return f'{self.nome}'


class Categoria(models.Model):
    
    nome = models.CharField(max_length=255)

    def __str__(self):
        return f'{self.nome}'


class Enciclopedia(models.Model):
    
    name = models.CharField(max_length=255)
    assunto = models.CharField(max_length=255)
    conteudo = models.TextField()
    
    tipocategoria = models.ForeignKey(
        Categoria,
        on_delete=models.CASCADE,
    )

    def __str__(self):
        return f'{self.name}'