from django.urls import path

from . import views

app_name = 'pets'
urlpatterns = [
    # path('login/', views.login, name='login'),
    path('animais/<int:pk>/', views.PetUpdateView.as_view(), name='singlepet'),
    path('animais/', views.PetListView.as_view(), name='petlist'),
    path('clinicalist/', views.ClinicaListView.as_view(), name='clinicalist'),
    path('veterinarios/', views.VetListView.as_view(), name='vetlistpets'),
    path('lembrete/', views.LembreteView.as_view(), name='lembrete'),
    path('inserlembrete/', views.create_lembrete, name='inserlembrete'),
    path('banco/', views.BancoView.as_view(), name='banco'),
    
    # path('<int:pk>/', views.PetDetailView.as_view(), name='principal'),
    path('criar/', views.create_pet, name='createpet'),
    path('criar/dono/', views.create_donopet, name='createdono'),
    path('criar/vet/', views.create_vet, name='createvet'),
    path('criar/clinica/', views.create_clinica, name='createclinica'),
    path('<int:pk>/', views.PetDetailView.as_view(), name='singlepetdetail'),
    path('dono/', views.DonoView.as_view(), name='dono'),
    path('vet/', views.VetView.as_view(), name='vet'),
    path('clinica/', views.ClinicaView.as_view(), name='clinica'),
    path('agenda/', views.AgendaView.as_view(), name='agenda'),
    path('inseragenda/', views.AgendaUpdateView.as_view(), name='inseragenda'),
    path('historico/<int:pk>', views.HistoricoDetailView.as_view(), name='singlepethistorico'),
    path('inserehistorico/<int:pet_id>', views.create_historico, name='inserehistorico'),
    path('enciclopedia/', views.EnciclopediaListView.as_view(), name='enciclopedia'),
    path('enciclopedia/<int:pk>/', views.ArtigoDetailView.as_view(), name='artigo'),

    path('search/', views.search_all, name='search'), 
    
    
]