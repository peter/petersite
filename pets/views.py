from django.shortcuts import render, get_object_or_404
from django.views import generic
from django.views.generic.edit import UpdateView
from django.http import HttpResponseRedirect
from django.urls import reverse, reverse_lazy
from .models import Pet, Animal, Donopet, Veterinario, Clinica, Atividade, Historico, Enciclopedia, Categoria
from .forms import PetForm, DonopetForm, VeterinarioForm, ClinicaForm, AtividadeForm, HistoricoForm, VeterinarioparadonoForm
from django.contrib.auth.decorators import login_required, permission_required, user_passes_test
from django.contrib.auth.mixins import LoginRequiredMixin, PermissionRequiredMixin




# Create your views here.


class PetUpdateView(LoginRequiredMixin, PermissionRequiredMixin, generic.UpdateView):
    model = Pet
    template_name = 'pets/singlepet.html'
    fields = ['name', 'tipo_sanguineo', 'raca','imagem', 'data_de_nascimento']
    success_url = './'
    permission_required = 'pets.atualizar_pet'

class PetDetailView(LoginRequiredMixin, generic.DetailView):
    model = Pet
    template_name = 'pets/singlepetdetail.html'

class PetListView(LoginRequiredMixin, generic.ListView):
    model = Pet
    template_name = 'pets/petlist.html'
    success_url = '/animais'

class ClinicaListView(LoginRequiredMixin, generic.ListView):
    model = Pet
    template_name = 'pets/clinicalist.html'
    success_url = '/clinicalist'
    
class VetListView(LoginRequiredMixin, generic.ListView):
    model = Atividade
    template_name = 'pets/vetlistpets.html'
    success_url = '/veterinarios'

class LembreteView(LoginRequiredMixin, generic.ListView):
    model = Pet
    template_name = 'pets/lembrete.html'
    success_url = '/pets/lembrete'


def create_lembrete(request):
    if request.method == 'POST':
        form = AtividadeForm(request.POST)
        if form.is_valid():
            atividade = Atividade(**form.cleaned_data) 
            atividade.usuario = request.user
            atividade.save()
           
        return HttpResponseRedirect(
            reverse('pets:lembrete'))
    else:
        form = AtividadeForm()
    context = {'form': form}
    return render(request, 'pets/inserlembrete.html', context)


class DonoView(LoginRequiredMixin, PermissionRequiredMixin, generic.ListView):
    model = Pet
    template_name = 'pets/dono.html'
    success_url = '/pets/dono'
    permission_required = 'pets.ver_dono'

class VetView(LoginRequiredMixin, PermissionRequiredMixin, generic.ListView):
    model = Pet
    template_name = 'pets/vet.html'
    success_url = '/pets/vet'
    permission_required = 'pets.ver_vet'

class ClinicaView(LoginRequiredMixin, PermissionRequiredMixin, generic.ListView):
    model = Pet
    template_name = 'pets/clinica.html'
    success_url = '/pets/clinica'
    permission_required = 'pets.ver_clinica'

class AgendaView(LoginRequiredMixin, generic.ListView):
    model = Pet
    template_name = 'pets/agenda.html'
    success_url = '/pets/agenda'

class AgendaUpdateView(LoginRequiredMixin, generic.ListView):
    model = Pet
    template_name = 'pets/inseragenda.html'
    success_url = '/pets/inseragenda'

class BancoView(LoginRequiredMixin, generic.ListView):
    model = Pet
    template_name = 'pets/banco.html'
    success_url = '/pets/banco'



class PetCreateView(LoginRequiredMixin, generic.CreateView):
    model = Pet
    fields = ['name', 'tipo_sanguineo', 'raca','imagem', 'data_de_nascimento']
    template_name = 'pets/createpet.html'
    success_url = '/pets/animais'

class DonopetCreateView(LoginRequiredMixin, PermissionRequiredMixin, generic.CreateView):
    model = Donopet
    fields = ['endereco', 'bairro', 'municipio','estado', 'cpf','celular','genero','data_de_nascimento','imagem']
    template_name = 'pets/createdono.html'
    success_url = '/pets/animais'
    permission_required = 'pets.preencher_forms_completar_signup_dono'

@permission_required('pets.preencher_forms_completar_signup_dono')
def create_donopet(request):
    if request.method == 'POST':
        form = DonopetForm(request.POST)
        if form.is_valid():
            donopet = Donopet(**form.cleaned_data) 
            donopet.usuario = request.user
            donopet.save()
           
        return HttpResponseRedirect(
            reverse('pets:dono'))
    else:
        form = DonopetForm()
    context = {'form': form}
    return render(request, 'pets/createdono.html', context)

@permission_required('pets.preencher_forms_completar_signup_vet')
def create_vet(request):
    if request.method == 'POST':
        form = VeterinarioForm(request.POST)
        if form.is_valid():
            veterinario = Veterinario(**form.cleaned_data) 
            veterinario.usuario = request.user
            veterinario.save()
           
        return HttpResponseRedirect(
            reverse('pets:vet'))
    else:
        form = VeterinarioForm()
    context = {'form': form}
    return render(request, 'pets/createvet.html', context)

@permission_required('pets.preencher_forms_completar_signup_clinica')
def create_clinica(request):
    if request.method == 'POST':
        form = ClinicaForm(request.POST)
        if form.is_valid():
            clinica = Clinica(**form.cleaned_data) 
            clinica.usuario = request.user
            clinica.save()
           
        return HttpResponseRedirect(
            reverse('pets:clinica'))
    else:
        form = ClinicaForm()
    context = {'form': form}
    return render(request, 'pets/createclinica.html', context)

def create_historico(request, pet_id):
    pet = get_object_or_404(Pet, pk=pet_id)
    if request.method == 'POST':
        form = HistoricoForm(request.POST)
        if form.is_valid():
            historico = Historico(**form.cleaned_data) 
            historico.editor = request.user
            historico.pet = pet
            historico.save()
            return HttpResponseRedirect(
                reverse('pets:singlepethistorico', args=(pet_id, )))
    else:
        form = HistoricoForm()
    context = {'form': form, 'pet': pet}
    return render(request, 'pets/inserehistorico.html', context)

class HistoricoDetailView(generic.DetailView):
    model = Pet
    template_name = 'pets/singlepethistorico.html'





# def update_historico(request, pet_id, hist_id):
#     pet = get_object_or_404(Pet, pk=pet_id)
#     hist = get_object_or_404(Pet, pk=pet_id)

#     if requested.method == "POST":
#         historico = Historico(**form.cleaned_data) 
#             historico.editor = request.user
#             historico.pet = pet
#             historico.save()
#             return HttpResponseRedirect(
#                 reverse('pets:singlepet', args=(pet_id, )))
#         context = {'pet':pet}
#         return render(request, 'pets/atualizahistorico.html', context)

# def delete_historico(request, pet_id):
#     pet = get_object_or_404(Pet, pk=pet_id)
#     if request.method == "POST":
#         pet.delete()
#         return HttpResponseRedirect(reverse('pets:singlepethistorico'))
#     context = {'pet':pet}
#     return render(request, 'pets/deletehistorico.html', context)
    
    


# class PetCreateView(LoginRequiredMixin, generic.CreateView):
#     model = Pet
#     fields = ['name', 'tipo_sanguineo', 'raca','imagem', 'data_de_nascimento', 'tipo_animal']
#     template_name = 'pets/createpet.html'
#     success_url = '/pets/animais'



@login_required
@permission_required('pets.criar_pet')
def create_pet(request):
    if request.method == 'POST':
        pet_form = PetForm(request.POST)
        if pet_form.is_valid():
            pet = Pet(**pet_form.cleaned_data) 
            pet.dono = request.user
            pet.save()
           
        return HttpResponseRedirect(
            reverse('pets:singlepet', args=(pet.pk, )))
    else:
        pet_form = PetForm()
    context = {'pet_form': pet_form}
    return render(request, 'pets/createpet.html', context)


def search_all(request):
    context = {}
    if request.GET.get('query', False):
        search_term = request.GET['query'].lower()
        pet_list = Pet.objects.filter(name__icontains=search_term)
        vet_list = Veterinario.objects.filter(nome__icontains=search_term)
        clinica_list = Clinica.objects.filter(nome__icontains=search_term)
        context = {"pet_list": pet_list, "vet_list":vet_list, "clinica_list":clinica_list}

    # if request.method == 'POST':
    #     form = AtividadeForm(request.POST)
    #     if form.is_valid():
    #         atividade = Atividade(**form.cleaned_data) 
    #         atividade.usuario = request.user
    #         atividade.vet = request.veterinario
    #         atividade.save()
            
    #     return HttpResponseRedirect(
    #         reverse('pets:lembrete'))
    # else:
    #     form = AtividadeForm()
    # context['form'] = form
    
    return render(request, 'pets/search.html', context)


# def create_lembrete(request):
#     if request.method == 'POST':
#         form = AtividadeForm(request.POST)
#         if form.is_valid():
#             atividade = Atividade(**form.cleaned_data) 
#             atividade.usuario = request.user
#             atividade.save()
           
#         return HttpResponseRedirect(
#             reverse('pets:lembrete'))
#     else:
#         form = AtividadeForm()
#     context = {'form': form}
#     return render(request, 'pets/inserlembrete.html', context)


class EnciclopediaListView(LoginRequiredMixin, generic.ListView):
    model = Enciclopedia
    template_name = 'pets/enciclopedias.html'
    

class ArtigoDetailView(LoginRequiredMixin, generic.DetailView):
    model = Enciclopedia
    template_name = 'pets/artigo.html'

